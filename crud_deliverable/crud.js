var dbTemplate = (function(params) {
  // Data
  var people = [];
  // people = ["Antonio", "Benjamin", "Christian"];

  // Cache
  var $container = $(`#${params.element_id}`);
  var $el = {
	  list: $container.find('ul.js-list'),
	  input: $container.find('input.js-person-name'),
	  submit: $container.find('button.js-submit')
  }

  // Render List of items in UI
  function render() {
    $el.list.html("");

    people.forEach( (x) => 
      {
        var li = document.createElement("li");
        li.setAttribute("class", "list__item")

          var span = document.createElement("span");
          span.appendChild(document.createTextNode(x));
          li.appendChild(span);

          var iClass = document.createElement("i");
          iClass.setAttribute("class", "del");
          iClass.appendChild(document.createTextNode("x"));
          li.appendChild(iClass);

        $el.list.append(li);
      }
    )
  }

  // Bind Submit and Delete Buttons
  function bind() {
    $el.submit.on('click', submitButton);
    $el.list.delegate('i.del', 'click', deleteButton);
  }

  // API Functions
  function addPerson(value) {
    people.push(value);
    render();
  }
  function deletePerson(index) {
    people.splice(index, 1);
    render();
  }
  function init() {
    render();
    bind();
  }

  // UI Buttons
  function submitButton() {
    var addVal = $el.input.val();
    $el.input.val('');
    addPerson(addVal);
  }
  function deleteButton(e) {
    var li_target = e.target.closest("li");
    var i = $el.list.find('li').index(li_target);
    deletePerson(i);
  }

  // Initialization
  init();

  // Return API Calls
  return {
    addPerson,
    deletePerson,
    init
  }
});

// Creates Containers on Page
var data = (function createContainer() {

  // Create Structure of Container
  // Adds div, containing input and submit buttons
  // Adds ul, which contains no li at creation
  function setElements(element_id) {
    var container = document.getElementById(element_id);

      var div = document.createElement("div");
      div.setAttribute("class", "input-group");

        var input = document.createElement("input");
        input.setAttribute("placeholder", "name");
        input.setAttribute("class", "js-person-name input-group__input");
        div.appendChild(input);

        var button = document.createElement("button");
        button.setAttribute("class", "js-submit button input-group__button");
        button.appendChild(document.createTextNode("Add Person"));
        div.appendChild(button);

      var ul = document.createElement("ul");
      ul.setAttribute("class", "js-list list");

    container.appendChild(div);
    container.appendChild(ul);
  }

  // create array, which will store each set of modules on the page
  var db = [];

  // loop through matching containers to create modules and associated dbs
  var containersInHTML = document.getElementsByClassName("container");
  for(var i = 0; i < containersInHTML.length; i++){
    var temp_id = containersInHTML[i].id;
    setElements(temp_id);
    db.push(dbTemplate({element_id: temp_id}));
  }

  return db;

})();